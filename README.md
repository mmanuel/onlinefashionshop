Proces kreiranja tablica u bazi:
1. Otvoriti online_fashion_shop kao projekt
2. Instalirati pip
3. U terminalu (pozicionirati se u online_fashion shop) 
    izvrsiti:  "pip install -r requirements.txt"
4. Skinuti i instalirati postgres (zapamtiti username i password)
5. U config.ini promjeniti podatke -> host ostaje localhost, database
 je svejedno koji stavite (bitno je da nije isti naziv neke druge baze koje imate lokalno),
 user je vecinom postgres (ako niste nista mjenjali prilikom instalacije)...
6. Pokrenite db_main
7. Ako je db_main prosao bez greski, refreshajte pgadmin i provjerite jesu li svi podaci u tablicama