from sqlalchemy import Column, Table, BigInteger, Text, Boolean, ForeignKey, String, Integer, TIMESTAMP, Float, CHAR
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import sessionmaker
from ..utils import preload_data_raw_sql

def create_tables(engine, meta):

    Roles = Table("Roles", meta,
                  Column("RoleId", BigInteger, primary_key=True, nullable=False),
                  Column("RoleName", String),
                  extend_existing = True)
    Roles.create(bind=engine, checkfirst=True)

    Users = Table("Users", meta,
                  Column("UserId", UUID(as_uuid=True), unique=True, nullable=False, primary_key=True),
                  Column("Email", String, unique=True),
                  Column("FirstName", String),
                  Column("LastName", String),
                  Column("Address", String),
                  Column("PasswordHash", Text),
                  Column("EmailConfirmed", Boolean),
                  Column("IsActive",Boolean),
                  extend_existing = True
                  )
    Users.create(bind=engine, checkfirst=True)

    UserRoles = Table("UserRoles", meta,
                      Column("UserRoleId", BigInteger, primary_key=True),
                      Column("UserId", UUID(as_uuid=True), ForeignKey("Users.UserId"), nullable=False),
                      Column("RoleId", BigInteger, ForeignKey("Roles.RoleId"), nullable=False),
                      extend_existing=True)
    UserRoles.create(bind=engine, checkfirst=True)

    Categories = Table("Categories", meta,
                  Column("CategoryId", BigInteger, unique=True, primary_key=True, nullable=False, autoincrement=True),
                  Column("CategoryName", String ),
                  Column("IsActive",Boolean),
                  extend_existing=True)
    Categories.create(bind=engine, checkfirst=True)

    Sizes = Table("Sizes", meta,
                      Column("SizeId", BigInteger, unique=True, primary_key=True, nullable=False, autoincrement=True),
                      Column("SizeName", CHAR),
                      extend_existing=True)
    Sizes.create(bind=engine, checkfirst=True)

    Products = Table("Products", meta,
                 Column("ProductId", BigInteger, nullable=False, primary_key=True),
                 Column("ProductName", String),
                 Column("ProductPrice", Float),
                 Column("ImgURL", Text),
                 Column("Description", String),
                 Column("SizeId", BigInteger, ForeignKey("Sizes.SizeId"), nullable=False, primary_key=True),
                 Column('IsActive',Boolean),
                 extend_existing=True)
    Products.create(bind=engine, checkfirst=True)

    ProductCategories = Table("ProductCategories", meta,
                  Column("ProductCategoryId", BigInteger, primary_key=True, unique=True, nullable=False, autoincrement=True),
                  Column("ProductId", BigInteger),
                  Column("CategoryId", BigInteger, ForeignKey("Categories.CategoryId")),
                  extend_existing=True)
    ProductCategories.create(bind=engine, checkfirst=True)

    Coupons = Table('Coupons', meta,
                      Column('CouponId', BigInteger, unique=True, nullable=False, primary_key=True, autoincrement=True),
                      Column('Percentage', Integer),
                      Column('Expires', String),
                      Column('Amount', BigInteger),
                      Column('Code', String),
                      Column("IsActive", Boolean),
                      extend_existing=True)
    Coupons.create(bind=engine, checkfirst=True)

    UserBaskets = Table('UserBaskets', meta,
                    Column('UserBasketId', BigInteger, unique=True, nullable=False, primary_key=True, autoincrement=True),
                    Column('UserId', UUID(as_uuid=True), ForeignKey("Users.UserId"), primary_key=True, nullable=False),
                    Column('IsActive', Boolean),
                    extend_existing=True)
    UserBaskets.create(bind=engine, checkfirst=True)

    Baskets = Table('Baskets', meta,
                    Column('BasketId', BigInteger, unique=True, nullable=False, primary_key=True, autoincrement=True),
                    Column('ProductId', BigInteger, nullable=False),
                    Column('SizeId', BigInteger, ForeignKey("Sizes.SizeId"),  nullable=False),
                    Column('Amount', BigInteger, nullable=False),
                    Column('UserBasketId', BigInteger, ForeignKey("UserBaskets.UserBasketId")),
                    Column('IsActive', Boolean),
                    extend_existing = True)
    Baskets.create(bind=engine, checkfirst=True)

    Stock = Table('Stock', meta,
                        Column('StockId', BigInteger, primary_key=True),
                        Column('StockProductId', BigInteger),
                        Column('StockSizeId', BigInteger),
                        Column('Amount', BigInteger),
                        Column('IsActive', Boolean),
                        extend_existing=True)
    Stock.create(bind=engine, checkfirst=True)

    Bills = Table('Bills', meta,
                  Column('BillId', BigInteger, unique=True, nullable=False,primary_key=True, autoincrement=True),
                  Column('PDV', BigInteger),
                  Column('ZKI', String),
                  Column('JIR', String),
                  Column('TimeDate', String),
                  Column('Price', BigInteger),
                  Column("IsActive", Boolean),
                  extend_existing=True)
    Bills.create(bind=engine, checkfirst=True)

    Purchases = Table('Purchases', meta,
                        Column('PurchaseId', BigInteger, unique=True, nullable=False, primary_key=True,
                               autoincrement=True),
                        Column('CouponId', BigInteger, ForeignKey("Coupons.CouponId"), nullable=True),
                        Column('BillId', BigInteger, ForeignKey("Bills.BillId")),
                        Column('UserBasketId', BigInteger, ForeignKey("UserBaskets.UserBasketId")),
                        extend_existing=True)
    Purchases.create(bind=engine, checkfirst=True)



    tables_for_preload = [Roles, Users, UserRoles, Categories, Sizes, Products, ProductCategories, Coupons, UserBaskets,
                          Baskets, Stock, Bills, Purchases]

    sql_files = ['Roles.txt', 'Users.txt', 'UserRoles.txt', 'Categories.txt', 'Sizes.txt', 'Products.txt',
                 'ProductCategories.txt']

    Session = sessionmaker(bind=engine)
    session = Session()

    for table, file in zip(tables_for_preload, sql_files):
        preload_data_raw_sql(session, table, file)


    session.close()




