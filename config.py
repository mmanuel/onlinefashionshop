

class Config(object):
    DEBUG = False
    TESTING = False


class ProductionConfig(Config):
    #TODO za produkciju
    pass


class DevelopmentConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
