from flask import Flask, jsonify, request, render_template, current_app, copy_current_request_context
from flask.views import MethodView
from Database.db_api import *
from config import TestingConfig
from flask_jwt_extended import (JWTManager, jwt_required, create_access_token, get_jwt_identity)
from flask_cors import CORS
from flask_mail import Message, Mail
from threading import Thread
from requests.exceptions import HTTPError
import os
import requests

app = Flask(__name__)
app.config.from_object(TestingConfig())
CORS(app)


# https://flask-jwt-extended.readthedocs.io/en/stable/basic_usage/
app.config['JWT_SECRET_KEY'] = 'super-secret'  # Change this!
jwt = JWTManager(app)

###MAIL###
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'registerfashionshop@gmail.com'
app.config['MAIL_PASSWORD'] = 'onlinefashionshop'
app.config['MAIL_USE_SSL'] = True
app.config['DEFAULT_MAIL_SENDER'] = 'registerfashionshop@gmail.com'
app.config['SECRET_KEY'] = 'super-secret'
app.config['DEBUG'] = True
mail = Mail(app)

###UPLOAD_FOLDER###
UPLOAD_FOLDER = './Database/UploadFolder'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


###S3###
app.config['S3_BUCKET']= 'onlinefashionshop'
app.config['S3_KEY']= 'AKIAIQCAZFD2LO7RPPZQ'
app.config['S3_SECRET_ACCESS_KEY']= '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'
app.config['S3_LOCATION']= 'http://{}.s3.amazonaws.com/'.format(app.config['S3_BUCKET'])


class HelloWorld(MethodView):
    def get(self):
        return jsonify(welcome="hello")


app.add_url_rule("/", view_func=HelloWorld.as_view("hello_world"))


class Login(MethodView):
    # GET USERNAME AND PASSWORD IN BODY
    def post(self):
        if not request.is_json:
            print("REQ", request.data)
            return jsonify({"msg": "Missing JSON in request"}), 400
        email = request.json.get('email')
        password = request.json.get('password')

        login_data, error = validate_login(email, password)
        if error is None:
            data = {'user_id': login_data[0], 'email': login_data[1], 'first_name': login_data[2],
                    'last_name': login_data[3], 'email_confirmed': login_data[4], 'role': login_data[5]}
            access_token = create_access_token(identity=data, expires_delta=False)
            return jsonify(token=access_token, error=None), 200
        else:
            print(error)
            return jsonify(token=None, error=error), 400


app.add_url_rule("/login", view_func=Login.as_view("login"))


class Registration(MethodView):
    def post(self):
        print(request.json)
        args = ['email', 'firstName', 'lastName', 'address', 'password']
        if all(args[i] in request.json for i in range(len(args))):
            data, error = post_user(request.json)
            if error is None:
                data, error = registration_mail(request.json['email'], request.json['firstName'], data[1])
                return jsonify(data=data, error=error), 201
            else:
                return jsonify(data=data, error=error), 400
        else:
            return jsonify(data=None, error="Missing arguments"), 400


app.add_url_rule("/registration", view_func=Registration.as_view("registration"))


def registration_mail(email, first_name, id):
    msg = Message("Registration link for OnlineFashionShop",
                  sender="registerfashionshop@gmail.com",
                  recipients=[email])
    msg.html = render_template('mail.html', first_name=first_name, uuid=id)

    try:
        mail.send(msg)
        return 'Success', None
    except Exception as e:
        return None, e




class Activate(MethodView):
    def get(self, id):
        data, error = activate_account(id)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400


app.add_url_rule("/activate/<uuid:id>", view_func=Activate.as_view("activate"))


class EditProfile(MethodView):
    @jwt_required
    def post(self):
        userid = get_jwt_identity()["user_id"]
        data, error = edit_profile(request.json, userid)
        if error is None:
            data = {'user_id': data[0], 'email': data[1], 'first_name': data[2],
                    'last_name': data[3], 'email_confirmed': data[4], 'role': data[5]}
            access_token = create_access_token(identity=data, expires_delta=False)
            return jsonify(token=access_token, error=None), 200
        else:
            return jsonify(data=data, error=error), 400


app.add_url_rule("/editprofile", view_func=EditProfile.as_view("editprofile"))


class Products(MethodView):
    def get(self, id=None):
        data, error = get_products(id)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400

    @jwt_required
    def post(self):
        amount = {
            "S" : request.form['amount_S'],
            "M" : request.form['amount_M'],
            "L" : request.form['amount_L'],
        }
        category =request.form['category_name']
        desc = request.form['desc']
        product_name = request.form['product_name']
        product_price = request.form['product_price']

        req = {
            "amount" : amount,
            "category_name" : category,
            "desc" : desc,
            "product_name" : product_name,
            "product_price" : product_price
        }
        print(req)
        if get_jwt_identity()['role'] == "BasicUser":
            return jsonify(data=None, error="You don't have authority to add products"), 401
        data, error = post_product(req,request.files)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400

    @jwt_required
    def put(self, id=None):
        amount = {
            "S" : request.form['amount_S'],
            "M" : request.form['amount_M'],
            "L" : request.form['amount_L'],
        }
        category = {
            "category_name" : request.form['category_name'],
            "category_id" : request.form['category_id'],
            "is_active" : request.form['is_active'],
        }
        desc = request.form['desc']
        id = request.form['id']
        product_name = request.form['product_name']
        product_price = request.form['product_price']

        req = {
            "amount" : amount,
            "category" : category,
            "desc" : desc,
            "id" : id,
            "product_name" : product_name,
            "product_price" : product_price
        }

        if get_jwt_identity()['role'] == "BasicUser":
            return jsonify(data=None, error="You don't have authority to update products"), 401
        data, error = put_product(req, id, request.files)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400

    @jwt_required
    def delete(self, id=None):
        if get_jwt_identity()['role'] == "BasicUser":
            return jsonify(data=None, error="You don't have authority to delete products"), 401
        data, error = delete_product(id)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400


app.add_url_rule("/products", view_func=Products.as_view("products"))
app.add_url_rule("/products/<int:id>", view_func=Products.as_view("product"), methods=['GET', 'PUT', 'DELETE'])


class Category(MethodView):
    def get(self, id=None):
        data, error = get_categories(id)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400

    @jwt_required
    def post(self):
        if get_jwt_identity()['role'] == "BasicUser":
            return jsonify(data=None, error="You don't have authority to add categories"), 401
        data, error = post_category(request.json)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400

    @jwt_required
    def delete(self, id=None):
        if get_jwt_identity()['role'] == "BasicUser":
            return jsonify(data=None, error="You don't have authority to delete category"), 401
        data, error = delete_category(id)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400


app.add_url_rule("/categories", view_func=Category.as_view("categories"))
app.add_url_rule("/categories/<int:id>", view_func=Category.as_view("category"))


class Bills(MethodView):
    def get(self, id=None):
        data, error = get_bills(id)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400

    @jwt_required
    def post(self):
        if get_jwt_identity()['role'] == "BasicUser":
            return jsonify(data=None, error="You don't have authority to add bills"), 401
        data, error = post_bills(request.json)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400

    @jwt_required
    def delete(self, id=None):
        if get_jwt_identity()['role'] == "BasicUser":
            return jsonify(data=None, error="You don't have authority to delete bill"), 401
        data, error = delete_bills(id)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400


app.add_url_rule("/bills", view_func=Bills.as_view("bills"))
app.add_url_rule("/bills/<int:id>", view_func=Bills.as_view("bill"))


class Baskets(MethodView):
    @jwt_required
    def get(self, id=None):
        userid = get_jwt_identity()['user_id']
        data, error = get_baskets(userid, id)  # id=IsActive
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400

    @jwt_required
    def post(self):
        userid = get_jwt_identity()['user_id']
        data, error = post_baskets(request.json, userid)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400

    @jwt_required
    def put(self):
        userid = get_jwt_identity()['user_id']
        data, error = put_baskets(request.json, userid)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400

    def delete(self, id=None):  # id=BasketId
        data, error = delete_baskets(id)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400


# app.add_url_rule("/baskets/<int:active>", view_func=Baskets.as_view("getbaskets"))
app.add_url_rule("/baskets", view_func=Baskets.as_view("baskets"))
app.add_url_rule("/baskets/<int:id>", view_func=Baskets.as_view("basket"))


class UserBaskets(MethodView):  # Za neaktivne košarice
    @jwt_required
    def get(self, id=None):
        userid = get_jwt_identity()['user_id']
        data, error = get_userbaskets(userid, id)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400

    def delete(self, id=None):  # id=UserBasketId
        data, error = delete_userbaskets(id)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400



app.add_url_rule("/userbaskets", view_func=UserBaskets.as_view("userbaskets"))
app.add_url_rule("/userbaskets/<int:id>", view_func=UserBaskets.as_view("userbasket"))


class Coupons(MethodView):
    def get(self, code=None):
        data, error = get_coupons(code)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400

    @jwt_required
    def post(self):
        print(request.json)
        if get_jwt_identity()['role'] == "BasicUser":
            return jsonify(data=None, error="You don't have authority to add coupons"), 401
        data, error = post_coupon(request.json)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400

    @jwt_required
    def delete(self, id=None):
        if get_jwt_identity()['role'] == "BasicUser":
            return jsonify(data=None, error="You don't have authority to delete coupon"), 401
        data, error = delete_coupon(id)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400


app.add_url_rule("/coupons", view_func=Coupons.as_view("coupons"))
app.add_url_rule("/coupons/<string:code>", view_func=Coupons.as_view("coupon"))
app.add_url_rule("/coupons/<int:id>", view_func=Coupons.as_view("coupon_delete"), methods=['DELETE'])


class Stock(MethodView):
    def get(self, id=None):
        data, error = get_stock(id)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400

    @jwt_required
    def post(self):
        if get_jwt_identity()['role'] == "BasicUser":
            return jsonify(data=None, error="You don't have authority to add to stock"), 401
        data, error = post_stock(request.json)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400

    @jwt_required
    def delete(self, id=None):
        if get_jwt_identity()['role'] == "BasicUser":
            return jsonify(data=None, error="You don't have authority to delete from stock"), 401
        data, error = delete_stock(id)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400


app.add_url_rule("/stock", view_func=Stock.as_view("stocks"))
app.add_url_rule("/stock/<int:id>", view_func=Stock.as_view("stock"))


class Holding(MethodView):  # NIJE JOŠ GOTOVO
    def get(self):
        err = None
        try:
            jir = requests.get("https://holding-fina.herokuapp.com/jir")
            array = jir.json()
            zki = requests.get("https://holding-fina.herokuapp.com/zki")
            array.update(zki.json())
            time = jir.headers['Date']
            array["time"] = time
        except HTTPError as err:
            print(f'HTTP error occurred: {err}')
            print(array)
        except Exception as err:
            print(f'Other error occurred: {err}')
        if jir.status_code and zki.status_code:
            status = 200
        else:
            status = 400
        return jsonify(data=array, error=err), status


app.add_url_rule("/holding", view_func=Holding.as_view("holding"))


class Purchases(MethodView):
    @jwt_required
    def get(self):
        if get_jwt_identity()['role'] == "BasicUser":
            return jsonify(data=None, error="You don't have authority to view purchases"), 401
        data, error = get_purchases()
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400


app.add_url_rule("/purchases", view_func=Purchases.as_view("purchases"))


class Users(MethodView):
    @jwt_required
    def get(self):
        if get_jwt_identity()['role'] == "BasicUser":
            return jsonify(data=None, error="You don't have authority to see users"), 401
        data, error = get_users()
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400

    @jwt_required
    def put(self):
        if get_jwt_identity()['role'] == "BasicUser":
            return jsonify(data=None, error="You don't have authority to update users"), 401
        data, error = put_users(request.json)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400

    @jwt_required
    def delete(self, id=None):
        if get_jwt_identity()['role'] == "BasicUser":
            return jsonify(data=None, error="You don't have authority to delete users"), 401
        data, error = delete_user(id)
        if error is None:
            return jsonify(data=data, error=error), 200
        else:
            return jsonify(data=data, error=error), 400


app.add_url_rule("/users", view_func=Users.as_view("users"))
app.add_url_rule("/users/<uuid:id>", view_func=Users.as_view("user"))


class Search(MethodView):

    def get(self):
        string = request.query_string.decode('utf-8')
        args = string.replace(string[:5], '').split('+')
        data, error = search_products(args)
        return jsonify(data=data, error=error)

app.add_url_rule("/search", view_func=Search.as_view("search"))


class Checkout(MethodView):

    @jwt_required
    def post(self):
        user_id = get_jwt_identity()['user_id']
        data, error = checkout(request.json, user_id)
        print(error)
        if data is not None:
            products, error = get_products_by_data(data[0])
            confirmation_mail(request.json['email'], request.json['firstName'], products, data[1], data[2], data[3], data[4])
        if error is None:
            return jsonify(data='Check email for bill', error=error), 200
        else:
            return jsonify(data=data, error=error), 400


app.add_url_rule("/baskets/checkout", view_func=Checkout.as_view("checkout"))


def confirmation_mail(email, first_name, products, total_price, zki, jir, time):
    msg = Message("Order confirmation for OnlineFashionShop",
                  sender="registerfashionshop@gmail.com",
                  recipients=[email])
    msg.html = render_template('shop.html', first_name=first_name, len=len(products), products=products, total_price=total_price, zki=zki, jir=jir, time=time)

    @copy_current_request_context
    def send_mail(msg):
        mail.send(msg)

    Thread(target=send_mail, args=(msg,)).start()



if __name__ == '__main__':
    # port = int(os.environ.get("PORT", 5000))
    # app.run(host='0.0.0.0', port=port)
    app.run()
