import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import HomePage from "./pages/HomePage";
import Login from "./pages/Login";
import Registration from "./pages/Registration";
import Products from "./pages/Products";
import Profil from "./pages/Profil";
import Categories from "./pages/Categories";
import Basket from "./pages/Basket";
import BasketHistory from "./pages/BasketHistory";
import BasketHistoryById from "./pages/BasketHistoryById";
import Checkout from "./pages/Checkout";
import Admin from "./admin/Admin";
import CategoriesAdmin from "./admin/CategoriesAdmin";
import ProductDisplayAdmin from "./admin/ProductsDisplayAdmin";
import IncomeAdmin from "./admin/IncomeAdmin";
import BillsAdmin from "./admin/BillsAdmin";
import CouponsAdmin from "./admin/CouponsAdmin";
import ProductById from "./pages/ProductById";
import Footer from "./components/Footer";
import Header from "./components/Header";
import NavBar from "./components/NavBar";
import InfoAdmin from "./admin/InfoAdmin";
import Owners from "./admin/Owners";
import { Redirect } from "react-router-dom";
import CategoryById from "./pages/CategoryById";
import SearchResults from "./pages/SearchResults";

class App extends React.Component {
  render() {
    return (
      <Router>
        <NavBar />
        <Switch>
          <Route exact path="/">
            <HomePage />
            <Footer />
          </Route>
          <Route exact path="/baskets">
            <Header />
            <Basket />
            <Footer />
          </Route>
          <Route exact path="/baskets/checkout">
            <Checkout />
            <Footer />
          </Route>
          <Route exact path="/baskets/unactive">
            <Header />
            <BasketHistory />
            <Footer />
          </Route>
          <Route path="/baskets/unactive/">
            <Header />
            <BasketHistoryById />
            <Footer />
          </Route>
          <Route exact path="/categories">
            <Header />
            <Categories />
            <Footer />
          </Route>
          <Route path="/categories">
            <Header />
            <CategoryById />
            <Footer />
          </Route>
          <Route path="/search">
            <Header />
            <SearchResults />
            <Footer />
          </Route>
          <Route exact path="/products">
            <Header />
            <Products />
            <Footer />
          </Route>
          <Route path="/products/">
            <Header />
            <ProductById />
            <Footer />
          </Route>
          <Route exact path="/login">
            <Login />
            <Footer />
          </Route>
          <Route exact path="/profile">
            <Profil />
            <Footer />
          </Route>
          <Route exact path="/registration">
            <Registration />
            <Footer />
          </Route>
          <Route
            exact
            path="/admin"
            render={() =>
              (localStorage.getItem("token") != null &&
                localStorage.getItem("role") === "SystemAdmin") ||
              localStorage.getItem("role") === "Owner" ? (
                <Admin />
              ) : (
                <Redirect to="/" />
              )
            }
          />
          <Route
            exact
            path="/admin/products"
            render={() =>
              (localStorage.getItem("token") != null &&
                localStorage.getItem("role") === "SystemAdmin") ||
              localStorage.getItem("role") === "Owner" ? (
                <ProductDisplayAdmin />
              ) : (
                <Redirect to="/" />
              )
            }
          />
          <Route
            exact
            path="/admin/categories"
            render={() =>
              (localStorage.getItem("token") != null &&
                localStorage.getItem("role") === "SystemAdmin") ||
              localStorage.getItem("role") === "Owner" ? (
                <CategoriesAdmin />
              ) : (
                <Redirect to="/" />
              )
            }
          />
          <Route
            exact
            path="/admin/coupons"
            render={() =>
              (localStorage.getItem("token") != null &&
                localStorage.getItem("role") === "SystemAdmin") ||
              localStorage.getItem("role") === "Owner" ? (
                <CouponsAdmin />
              ) : (
                <Redirect to="/" />
              )
            }
          />
          <Route
            exact
            path="/admin/owners"
            render={() =>
              (localStorage.getItem("token") != null &&
                localStorage.getItem("role") === "SystemAdmin") ||
              localStorage.getItem("role") === "Owner" ? (
                <Owners />
              ) : (
                <Redirect to="/" />
              )
            }
          />
          <Route
            exact
            path="/admin/income"
            render={() =>
              (localStorage.getItem("token") != null &&
                localStorage.getItem("role") === "SystemAdmin") ||
              localStorage.getItem("role") === "Owner" ? (
                <IncomeAdmin />
              ) : (
                <Redirect to="/" />
              )
            }
          />
          <Route
            exact
            path="/admin/bills"
            render={() =>
              (localStorage.getItem("token") != null &&
                localStorage.getItem("role") === "SystemAdmin") ||
              localStorage.getItem("role") === "Owner" ? (
                <BillsAdmin />
              ) : (
                <Redirect to="/" />
              )
            }
          />
          <Route
            exact
            path="/admin/info"
            render={() =>
              (localStorage.getItem("token") != null &&
                localStorage.getItem("role") === "SystemAdmin") ||
              localStorage.getItem("role") === "Owner" ? (
                <InfoAdmin />
              ) : (
                <Redirect to="/" />
              )
            }
          />
        </Switch>
      </Router>
    );
  }
}

export default App;
