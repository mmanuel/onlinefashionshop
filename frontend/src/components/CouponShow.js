import React, {Component} from "react";
import axios from "axios";
import CouponCard from "./CouponCard";

class CouponShow extends Component {
  state = {
    couponsList: [],
    show: false,
  };
  componentDidMount() {
    axios
      .get(window.backend + "/coupons")
      .then((response) => {
        console.log(response);
        this.setState({
          couponsList: response.data["data"],
        });
      })
      .catch((err) => console.log(err));
  }

  loadCoupons() {
      return this.state.couponsList.map((coupon) => (
        <CouponCard
            key={coupon.coupon_id}
            percentage={coupon.percentage}
            code={coupon.code}
        />
    ));
  }

  render() {
    return (
      <div className="couponbackground"
        style={{
            padding:"10% 20%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "#e8f4fc",
          boxSizing: "border-box",
        }}
      >
          {this.loadCoupons()}
      </div>
    );
  }
}

export default CouponShow;
