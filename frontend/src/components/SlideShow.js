import React, {useState} from 'react';
import { Link } from "react-router-dom";
import slika1 from "../images/version3mirror.png";
import slika2 from "../images/welcome.png";
import slika3 from "../images/shop.png";
import slika4 from "../images/shopmirror.png";
import {Button, Carousel, Col, Image, Row} from "react-bootstrap";
import './css/SlideShow.css';


function SlideShow() {
    const [index, setIndex] = useState(0);

    const handleSelect = (selectedIndex) => {
        setIndex(selectedIndex);
    };
    const style ={
        fontFamily: "Brush Script MT",
        fontSize: "5vw",
        fontWeight: "600",
        textAlign: "center",
        color: "#a6b8a6",
        textShadow: "2px 2px #013220, 2px 2px darkgreen, 2px 2px darkgreen",
    };

    return (
        <div
            style ={{
                marginTop: "3.2rem",
                backgroundColor: "#e8f4fc",
                textAlign: "center"
            }}
        >
        <Carousel activeIndex={index} onSelect={handleSelect} className="slideshow">
            <Carousel.Item className="slidepics" interval={4000} style={{paddingTop:"4.2rem", paddingBottom:"4.2rem"}}>
                <Row>
                    <Col xs={2}>
                        <Image src={slika1} alt="basket" height="100" width="100" style={{opacity: "0.9", marginTop:"90%"}}/>
                    </Col>
                    <Col>
                        <Image src={slika1} alt="basket" style={{opacity: "0.9", marginTop:"25%"}}/>
                    </Col>
                    <Col>
                        <Image className="collection" src={slika2} alt="basket" style={{ marginTop:"35%"}}/>
                    </Col>
                    <Col>
                        <Image src={slika3} alt="basket"  style={{opacity: "0.9", marginTop:"25%"}}/>
                    </Col>
                    <Col xs={2}>
                        <Image src={slika3} alt="basket" height="100" width="100" style={{opacity: "0.9", marginTop:"90%"}}/>
                    </Col>
                </Row>
                <Carousel.Caption>
                    <Link to="/products"><Button variant="dark" >Shop now!</Button></Link>
                </Carousel.Caption>
            </Carousel.Item>

            <Carousel.Item className="slidepics" interval={4000}>
                <Image src={slika4} alt="shopping" height="1100" width="1100" style={{opacity: "0.9"}}/>
                <Carousel.Caption>
                    <header style = {style} >GreenTeam WebShop</header>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
        </div>
    );
}

export default SlideShow;