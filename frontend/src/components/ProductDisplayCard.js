import React from "react";
import { Card, Nav } from "react-bootstrap";


function ProductDisplayCard(props) {
  return (
      <div
          style ={{
              marginLeft: "1rem",
              marginTop: "1.5rem",
              marginBottom: "1.5rem",
              display: "inline-block"
          }}
      >
        <Card style={{ width: "18rem" }} key={props.productId}>
          <Nav.Link href={"/products/" + props.productId}>
            <Card.Header>
              <Card.Img src={props.imgURL} style={{ width: "15rem", height: "18rem"}}/>
            </Card.Header>
            <Card.Body>
              <Card.Title style={{color:"black", textAlign:"center"}}>{props.productName}</Card.Title>
              <Card.Text style={{color:"black", textAlign:"center"}}>Price: {props.productPrice} HRK</Card.Text>
            </Card.Body>
          </Nav.Link>
        </Card>
      </div>
  );
}

export default ProductDisplayCard;
