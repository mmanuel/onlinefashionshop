import React from "react";
import { Link } from "react-router-dom";
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import KeyboardReturnIcon from '@material-ui/icons/KeyboardReturn';
import "./css/emptyBasket.css"

const EmptyBasket = ( ) => {
  return (
    <div className="empty-basket"
         style={{
             textAlign: "center",
             backgroundColor: "whitesmokes",
             border: "1px solid #e8f4fc"
         }}
    >
      <img className="swing"
        src="https://res.cloudinary.com/sivadass/image/upload/v1495427934/icons/empty-cart.png"
        alt="empty-cart"
        style={{
            border: "2px solid #000000"
        }}
      />
      <h2
          style={{
              marginTop: "2rem"
          }}
      >
          Empty basket! <SentimentVeryDissatisfiedIcon style={{fontSize: "2.5vw"}}/>
      </h2>
      <div>
        <p>
          Go back to <Link to="/products" style={{ color:"darkolivegreen", textDecoration: "underline" }}>products</Link> and add something!
        </p>
        <p>
          Or view your previous baskets:
            <li>
              <Link to="/baskets/unactive" style={{ color:"darkolivegreen", textDecoration: "underline" }}>
                  <KeyboardReturnIcon/>
                  Basket history...
              </Link>
            </li>
        </p>
      </div>
    </div>
  );
};

export default EmptyBasket;
