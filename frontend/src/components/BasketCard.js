import React from "react";
import { Component } from "react";
import { Button, Row, Col, Media } from "react-bootstrap";
import axios from "axios";
import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';

class BasketCard extends Component {
  subtrackFromBasket() {
    console.log("ja oduzimam");
    console.log(this.props.productName);

    const token = localStorage.getItem("token");

    axios
      .put(
        window.backend + "/baskets",
        {
          amount: this.props.amount - 1,
          product_id: this.props.productId,
          size_id: this.props.sizeId,
        },
        { headers: { Authorization: `Bearer ${token}` } }
      )
      .then((response) => {
        window.location.reload(false);
      })

      .catch((err) => console.log(err));
  }

  addToBasket() {
    const token = localStorage.getItem("token");

    axios
      .put(
        window.backend + "/baskets",
        {
          amount: this.props.amount + 1,
          product_id: this.props.productId,
          size_id: this.props.sizeId,
        },
        { headers: { Authorization: `Bearer ${token}` } }
      )
      .then((response) => {
        window.location.reload(false);
      })

      .catch((err) => console.log(err));
  }

  render() {
    const styles = {
      mediaItem: {

      },
      mediaItemButtons: {
        paddingTop: "2%",
        paddingBottom: "2%",
      },
    };
    return (
        <div style={{backgroundColor:"#e8f4fc"}}>
      <Media className={styles.mediaItem} style={{
        backgroundColor: "whitesmoke",
        marginLeft:"0.5%",
        marginRight:"0.5%",
        padding:"1%",
        border: "1px solid darkolivegreen",
      }}
      >
        <img
          width={150}
          height={150}
          className="align-self-center mr-3"
          src={this.props.imgURL}
          alt="Generic placeholder"
        />
        <Media.Body className={styles.mediaBody}>
          <p>
            <strong>
              <i>{this.props.productName}</i>
            </strong>
          </p>
          <Row>
            <Col xs={6}>
              <strong>
                {this.props.amount} piece(s) in size {this.props.sizeName}
              </strong>
            </Col>
            <Col xs={6}>
              <strong>Price: {this.props.productPrice * this.props.amount} kn</strong>
            </Col>
          </Row>

          <Row style={styles.mediaItemButtons}>
            <Col xs={6}>
              <Button
                onClick={() => this.subtrackFromBasket()}
                variant="danger"
                size="sm"
                style={{ backgroundColor: "#e06666", borderColor: "#e06666" }}
              >
                <RemoveIcon/>
              </Button>
              <Button
                onClick={() => this.addToBasket()}
                variant="success"
                size="sm"
                style={{ backgroundColor: "#b0cebc", borderColor: "#b0cebc" }}
              >
                <AddIcon/>
              </Button>
            </Col>
          </Row>
        </Media.Body>
      </Media>
          </div>
    );
  }
}

export default BasketCard;
