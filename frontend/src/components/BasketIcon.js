import React from "react";
import { Nav } from "react-bootstrap";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";


function BasketIcon() {
  return (
    <div>
      {localStorage.getItem("token") != null && (
        <Nav.Link href="/baskets" >
          <div>
            Basket <ShoppingCartIcon />
          </div>
        </Nav.Link>
      )}
    </div>
  );
}

export default BasketIcon;
