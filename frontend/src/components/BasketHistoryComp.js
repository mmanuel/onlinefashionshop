import BasketHistoryCard from "./BasketHistoryCard";
import axios from "axios";
import { Link } from "react-router-dom";
import React, { Component } from "react";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import LoyaltyIcon from "@material-ui/icons/Loyalty";
import RotateRightIcon from "@material-ui/icons/RotateRight";

class BasketHistoryComp extends Component {
  state = {
    basketState: [],
    loading: true,
  };

  componentDidMount() {
    console.log("usao u historyCOmp");
    const api = window.backend + "/userbaskets";
    const token = localStorage.getItem("token");
    axios
      .get(api, { headers: { Authorization: `Bearer ${token}` } })
      .then((res) => {
        console.log(res.data["data"]);
        this.setState({ loading: false });
        this.setState({ basketState: res.data["data"] });
      })
      .catch((err) => console.log(err));
  }

  render() {
    if (this.state.loading) {
      return (
        <div
          className="loading"
          style={{
            backgroundColor: "#e8f4fc",
            padding: "10%",
            textAlign: "center",
            fontSize: "2.5vw",
            fontStyle: "italic",
          }}
        >
          <RotateRightIcon
            style={{
              fontSize: "3vw",
            }}
          />
          Loading...
        </div>
      );
    }

    return (
      <div
        style={{
          backgroundColor: "#e8f4fc",
          border: "1px solid #e8f4fc",
        }}
      >
        {this.state.basketState.map((b) => {
          return (
            <BasketHistoryCard sum={b.sum} userBasketId={b.user_basket_id} />
          );
        })}

        <div
          style={{
            fontStyle: "italic",
            margin: "6%",
          }}
        >
          {Object.keys(this.state.basketState).length === 0 && (
            <div
              style={{
                textAlign: "center",
                fontSize: "2.5vw",
              }}
            >
              <p
                style={{
                  filter: "sepia(1) saturate(0)  hue-rotate(90deg)",
                  fontSize: "3vw",
                  fontStyle: "normal",
                }}
              >
                &#128533;
              </p>
              <p>You still haven't shopped here!</p>
              <p>
                Go back to{" "}
                <Link
                  to="/products"
                  style={{
                    color: "darkolivegreen",
                    textDecoration: "underline",
                  }}
                >
                  products <LoyaltyIcon style={{ fontSize: "2.5vw" }} />{" "}
                </Link>{" "}
                and spend some money!{" "}
                <AddShoppingCartIcon style={{ fontSize: "3vw" }} />
              </p>
            </div>
          )}
        </div>
      </div>
    );
  }
}
export default BasketHistoryComp;
