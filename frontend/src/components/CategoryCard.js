import React from "react";
import {Card, Button, Badge} from "react-bootstrap";

function CategoryCard(props) {
  return (
    <div
        style={{
            marginTop: "3%",
            marginBottom: "3%",
            marginLeft: "8%",
            display: "inline-block",
        }}
    >
      <Card
          style={{
              width: "18rem",
              borderColor:"#a6b8a6",
              color: "black",
          }}
          key={props.productId}
      >
        <Button

          size="lg"
          expand="lg"
          bg="dark"
          variant="dark"
          style={{
              textAlign: "center",
              backgroundColor:"#a6b8a6",
              borderColor:"#a6b8a6",
              opacity:"1",
              color:"black"
          }}
          disabled
        >
         Category
        </Button>

          <Card.Body
              style={{
              backgroundColor:"#fdeef4",
          }}>
              <Badge
                  className="collection"
                  pill
                  variant="secondary"
                  style={{
                      marginTop: "1rem",
                      display: "flex",
                      justifyContent: "center",
                      fontFamily: "Roboto",
                      color: "#a6b8a6",
                      backgroundColor: "#fcfdff",
                      textShadow: "-0.75px 0 black, 0 0.75px black, 0.75px 0 black, 0 -0.75px black",
                      border: "1.35px dashed #013220",

                  }}
              >
                  <h1>{props.category_name}</h1>
              </Badge>
          </Card.Body>
          <Card.Footer style={{
              display:"flex",
              alignItems:"center",
              justifyContent:"center",
              borderColor:"#fdeef4",
              backgroundColor:"#fdeef4",

          }}
          >
              <Button size="lg"
                      style={{
                          textAlign: "center",
                          backgroundColor:"#a6b8a6",
                          borderColor:"#a6b8a6",
                          marginBottom:"1rem",
                          color: "black"
                      }}
                      href={"/categories/" + props.categoryId}>
                  Shop now!
              </Button>
          </Card.Footer>
      </Card>
    </div>
  );
}

export default CategoryCard;
