import React, { Component } from "react";
import { Button, Card, Form, Modal } from "react-bootstrap";
import "./css/login.css";
import Axios from "axios";
import {Link} from "react-router-dom";

class Registration extends Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
    address: "",
    password: "",
    passwordRepeated: "",
    show: false,
    emailText: "",
    matchPasswords:"",
  };

  async handleSubmit(event) {
    event.preventDefault();
    if (this.state.password === this.state.passwordRepeated) {
      Axios.post(window.backend + "/registration", {
        email: this.state.email,
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        address: this.state.address,
        password: this.state.password,
      })
        .then((res) => {
          console.log(res);
          this.setState({show: true});
          this.setState({matchPasswords: ""});
        })
        .catch((err) => {
          console.log(err.response.message);
          if(this.state.email === ""){
            this.setState({emailText: "You must enter your e-mail!"});
          }
          else if(this.state.password !== this.state.passwordRepeated){
            this.setState({matchPasswords: "Passwords didn't match"})

          }
        });
    }
  }

  render() {
    const handleClose = () => this.setState({show: false});
    return (
      <div style={{ marginTop: "3rem" }}>
        <Card style={{ backgroundColor: "#e8f4fc", display:"flex", alignItems:"center", justifyContent:"center" }}>
          <Card.Body>
            <Form className="box" style={{ marginTop: "3rem" }}>
              <h3 style={{ textAlign: "center" }}>
                <b>Register</b>
              </h3>
              <Form.Group controlId="formBasicFirstName">
                <Form.Label>
                  <b>First name</b>
                </Form.Label>
                <Form.Control
                  type="first name"
                  placeholder="eg. Mark"
                  value={this.state.firstName}
                  onChange={(event) =>
                    this.setState({ firstName: event.target.value })
                  }
                />
              </Form.Group>
              <Form.Group controlId="formBasicLastName">
                <Form.Label>
                  <b>Last name</b>
                </Form.Label>
                <Form.Control
                  type="last name"
                  placeholder="eg. Smith"
                  value={this.state.lastName}
                  onChange={(event) =>
                    this.setState({ lastName: event.target.value })
                  }
                />
              </Form.Group>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>
                  <b>Email address</b>
                </Form.Label>
                <Form.Control
                  type="email"
                  placeholder="example@email.com"
                  value={this.state.email}
                  onChange={(event) =>
                    this.setState({ email: event.target.value })
                  }
                />
                <Form.Text className="text-muted">
                  We'll never share your email with anyone else.
                </Form.Text>
                <Form.Text className="text-danger">
                  {this.state.emailText}
                </Form.Text>
              </Form.Group>
              <Form.Group controlId="formBasicAddress">
                <Form.Label>
                  <b>Address</b>
                </Form.Label>
                <Form.Control
                  type="address"
                  placeholder="Enter address"
                  value={this.state.address}
                  onChange={(event) =>
                    this.setState({ address: event.target.value })
                  }
                />
                <Form.Text className="text-muted">
                  We'll never share your address with anyone else.
                </Form.Text>
              </Form.Group>
              <Form.Group controlId="formBasicPassword">
                <Form.Label>
                  <b>Password</b>
                </Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  value={this.state.password}
                  onChange={(event) =>
                    this.setState({ password: event.target.value })
                  }
                />
              </Form.Group>
              <Form.Group controlId="formBasicPasswordRepeated">
                <Form.Label>
                  <b>Repeat password</b>
                </Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Repeat password"
                  value={this.state.passwordRepeated}
                  onChange={(event) =>
                    this.setState({ passwordRepeated: event.target.value })
                  }
                />
                <Form.Text className="text-muted">
                  Passwords must match.
                </Form.Text>
                <Form.Text className="text-muted">
                  {this.state.matchPasswords}
                </Form.Text>
              </Form.Group>
              <span>
                <Button
                  className="submit-login-signin"
                  expand="lg"
                  bg="dark"
                  variant="dark"
                  type="submit"
                  onClick={(event) => this.handleSubmit(event)}
                >
                  Submit
                </Button>
              </span>
              <p>
                <br />
                Already have an account? Log in <a href={"/login"}>here</a>.
              </p>
            </Form>
          </Card.Body>
        </Card>
        <Modal
            show={this.state.show}
            onHide={handleClose}
            onShow={() => {

            }}
            centered
        >
          <Modal.Header closeButton style={{ backgroundColor: "#e3eee8" }}>
            <Modal.Title>
                Successful registration!</Modal.Title>
          </Modal.Header>
          <Modal.Body style={{ backgroundColor: "#e3eee8" }}>
            We have sent you an email where you must confirm your registration to continue using this profile.
            If you have already done so, press the button:
          </Modal.Body>
          <Modal.Footer style={{ backgroundColor: "#e3eee8" }}>
            <Link to="/login" className="btn btn-primary" style={{ backgroundColor: "#b0cebc", borderColor: "#b0cebc" }}>Login</Link>
          </Modal.Footer>

        </Modal>
      </div>
    );
  }
}

export default Registration;
