import "../components/css/HomePage.css";
import React from "react";
import SlideShow from "../components/SlideShow";
import CollectionShow from "../components/CollectionShow";
import CouponShow from "../components/CouponShow";

function HomePage() {
  return (
    <div className="App">
      <SlideShow />
      <CollectionShow/>
      <CouponShow/>
    </div>
  );
}

export default HomePage;
