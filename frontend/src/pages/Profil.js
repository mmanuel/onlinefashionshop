import React, { useState } from "react";
import { Form, Button, Modal, Card, Accordion, Row } from "react-bootstrap";
import Axios from "axios";
import jwt_decode from "jwt-decode";
import { Link } from "react-router-dom";
import axios from "axios";
import PersonPinIcon from "@material-ui/icons/PersonPin";
import { useHistory } from "react-router-dom";
import TouchAppIcon from "@material-ui/icons/TouchApp";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";
import DoneOutlineIcon from "@material-ui/icons/DoneOutline";
import SyncIcon from "@material-ui/icons/Sync";
import ArrowRightAltIcon from "@material-ui/icons/ArrowRightAlt";
import DeleteIcon from "@material-ui/icons/Delete";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";

function Profil() {
  const [showDelete, setShow] = useState(false);
  const handleCloseDelete = () => setShow(false);
  const handleShowDelete = () => setShow(true);
  const [firstName, setFirstName] = useState(
    localStorage.getItem("first_name")
  );
  const [lastName, setLastName] = useState(localStorage.getItem("last_name"));
  const [email, setEmail] = useState(localStorage.getItem("email"));

  const [changed, setChange] = useState(false);

  const [errFirstName, setErrFirstName] = useState("");
  const [errLastName, setErrLastName] = useState("");
  const [errEmail, setErrEmail] = useState("");
  let history = useHistory();
  console.log(firstName);
  console.log(errFirstName);
  console.log(errLastName);
  console.log(errEmail);

  async function handleSubmit() {
    const token = localStorage.getItem("token");
    if (firstName === "") setErrFirstName("Please enter your name!");
    else setErrFirstName("");
    if (lastName === "") setErrLastName("Please enter your last name!");
    else setErrLastName("");
    if (email === "") setErrEmail("Please enter email!");
    else setErrEmail("");
    if (firstName !== "" && lastName !== "" && email !== "") {
      Axios.post(
        window.backend + "/editprofile",
        {
          first_name: firstName,
          last_name: lastName,
          email: email,
        },
        { headers: { Authorization: `Bearer ${token}` } }
      )
        .then((res) => {
          if (res.data.error === null) {
            localStorage.setItem("token", res.data.token);
            const token = localStorage.getItem("token");
            let decoded = jwt_decode(token);
            localStorage.setItem("user_id", decoded.identity.user_id);
            localStorage.setItem("email", decoded.identity.email);
            localStorage.setItem("first_name", decoded.identity.first_name);
            localStorage.setItem("last_name", decoded.identity.last_name);
            localStorage.setItem(
              "email_confirmed",
              decoded.identity.email_confirmed
            );
            localStorage.setItem("role", decoded.identity.role);
            setChange(true);
          }
        })
        .catch((err) => console.log(err));
    }
  }
  async function handleDelete() {
    const token = localStorage.getItem("token");
    axios
      .delete(window.backend + "/users/" + localStorage.getItem("user_id"), {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        console.log(res);
        localStorage.removeItem("token");
        handleCloseDelete();
        history.push("/");
        window.location.reload(false);
      });
  }
  const styles = {
    backgroundColor: "whitesmoke",
    padding: "10%",
    marginTop: "3.2rem",
  };
  if (changed) {
    return (
      <div className="loading" style={styles}>
        <Row style={{ display: "flex", justifyContent: "center" }}>
          <h1>
            Changed{" "}
            <DoneOutlineIcon
              fontSize="large"
              style={{ marginBottom: "0.5rem" }}
            />{" "}
          </h1>
        </Row>
        <Row style={{ display: "flex", justifyContent: "center" }}>
          <Link
            to="/profile"
            onClick={() => window.location.reload()}
            style={{ color: "black" }}
          >
            <KeyboardBackspaceIcon /> Go back to profile!
          </Link>
        </Row>
      </div>
    );
  }

  return (
    <div
      style={{
        marginTop: "3rem",
        backgroundColor: "#e8f4fc",
        display: "flex",
        justifyContent: "center",
      }}
    >
      <Card
        className="profile-box"
        style={{ width: "37rem", marginTop: "3rem", marginBottom: "3rem" }}
      >
        <Card.Header
          style={{
            display: "flex",
            justifyContent: "center",
            backgroundColor: "#d9e0d9",
            fontSize: "1.5em",
          }}
        >
          <strong style=
                      {{
                        textDecoration: "underline overline",
                        fontSize: "2.5vw"
                      }}
          >
            Profile
          </strong>
        </Card.Header>
        <Card.Body style={{ textAlign: "center" }}>
          <PersonPinIcon
            style={{ fontSize: "10em", marginTop: "5%", color: "#303030" }}
          />
          <Card.Text>
              {firstName} {lastName}
          </Card.Text>
        </Card.Body>
        <Accordion>
          <Card>
            <Card.Header
              style={{
                backgroundColor: "#d9e0d9",
                display: "flex",
                justifyContent: "center",
              }}
            >
              <Accordion.Toggle
                as={Button}
                variant="link"
                eventKey="0"
                style={{ color: "#2f4c4f" }}
              >
                Change your profile
                <TouchAppIcon />
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="0">
              <Card.Body>
                <Form.Group className="profil-data">
                  <Form.Row style={{ marginBottom: "2rem" }}>
                    <div style={{ marginRight: "2px" }}>
                      <Form.Label htmlFor="fname" className="lab">
                        First name:
                      </Form.Label>
                      <Form.Control
                        className="change-data"
                        type="text"
                        placeholder="First name"
                        defaultValue={localStorage.getItem("first_name")}
                        onChange={(event) => setFirstName(event.target.value)}
                      />
                      <div className="text-danger">{errFirstName}</div>
                    </div>
                    <div style={{ marginRight: "0px" }}>
                      <Form.Label htmlFor="lname" className="lab">
                        Last name:
                      </Form.Label>
                      <Form.Control
                        className="change-data"
                        type="text"
                        placeholder="Last name"
                        defaultValue={localStorage.getItem("last_name")}
                        onChange={(event) => setLastName(event.target.value)}
                      />
                      <div className="text-danger">{errLastName}</div>
                    </div>
                  </Form.Row>
                  <div style={{ marginRight: "20px", marginBottom: "2rem" }}>
                    <Form.Label htmlFor="" className="lab">
                      E-mail adress:
                    </Form.Label>
                    <Form.Control
                      className="change-data"
                      type="text"
                      placeholder="E-mail adress"
                      defaultValue={localStorage.getItem("email")}
                      onChange={(event) => setEmail(event.target.value)}
                    />
                    <div className="text-danger">{errEmail}</div>
                  </div>

                  <Link
                    to="/profile"
                    style={{ display: "flex", justifyContent: "center" }}
                  >
                    <Button
                      className="uredi"
                      expand="lg"
                      bg="dark"
                      variant="dark"
                      type="submit"
                      onClick={() => handleSubmit()}
                    >
                      Change <SyncIcon />
                    </Button>
                  </Link>
                </Form.Group>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>
        <Card.Body style={{ backgroundColor: "#d9e0d9" }}>
          <Card.Subtitle style={{ textAlign: "center" }}>
            <Link to={"/baskets/unactive"} style={{ color: "#2f4c4f" }}>
              Go to basket history <ArrowRightAltIcon />
            </Link>
          </Card.Subtitle>
        </Card.Body>
        <Card.Body style={{ backgroundColor: "#d9e0d9" }}>
          <Card.Subtitle style={{ textAlign: "center" }}>
            If you want delete <DeleteIcon /> your profile >>>
            <Card.Link
                onClick={handleShowDelete}
                style={{ color: "#2f4c4f" }}
            >
              {" "}
              click here
            </Card.Link>
            {"<<<."}
          </Card.Subtitle>
        </Card.Body>
      </Card>
      <Modal
        show={showDelete}
        onHide={handleShowDelete}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton onClick={handleCloseDelete}>
          <Modal.Title>
            Delete User <DeleteForeverIcon />{" "}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {firstName}, are you sure you want to delete your profile?
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={handleCloseDelete}
            style={{ backgroundColor: "#e06666", borderColor: "#e06666" }}
          >
            No
          </Button>
          <Button
            onClick={() => handleDelete()}
            style={{ backgroundColor: "#88b8e7", borderColor: "#88b8e7" }}
          >
            Yes
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default Profil;
