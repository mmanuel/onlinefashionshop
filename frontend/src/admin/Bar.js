import React, {Component} from 'react';
import {ButtonGroup, Button, Nav, Card} from "react-bootstrap";


class Bar extends Component {
    render() {
        return (
            <Card style={{ width: '12rem', height: '35rem' }}>
                <ButtonGroup vertical>
                    <Button style={{backgroundColor:"#2f4c4f", borderColor: "#2f4c4f"}} >Edit</Button>
                    <Nav defaultActiveKey="/admin" className="flex-column">
                        <Nav.Link href="/admin/categories" style={{color:"black"}}>Categories</Nav.Link>
                        <Nav.Link href="/admin/coupons" style={{color:"black"}}>Coupons</Nav.Link>
                        <Nav.Link href="/admin/products" style={{color:"black"}}>Products</Nav.Link>
                        <Nav.Link href="/admin/owners" style={{color:"black"}}>Shop owners</Nav.Link>
                    </Nav>
                    <Button style={{backgroundColor:"#2f4c4f", borderColor: "#2f4c4f"}}>Statistics</Button>
                    <Nav defaultActiveKey="/admin" className="flex-column">
                        <Nav.Link href="/admin/bills" style={{color:"black"}}>Bills</Nav.Link>
                        <Nav.Link href="/admin/income" style={{color:"black"}}>Income</Nav.Link>
                    </Nav>
                    <Button style={{backgroundColor:"#2f4c4f", borderColor: "#2f4c4f"}}>Info</Button>
                    <Nav defaultActiveKey="/admin" className="flex-column">
                        <Nav.Link href="/admin/info" style={{color:"black"}}>User info</Nav.Link>
                    </Nav>
                </ButtonGroup>
            </Card>

        );
    }
}

export default Bar;