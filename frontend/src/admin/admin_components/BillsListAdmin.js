import React, { Component } from "react";
import Card from "./BillAdmin";
import axios from "axios";

class BillsListAdmin extends Component {
  state = {
    billsList: [],
  };

  componentDidMount() {
    axios.get(window.backend + "/bills").then((response) => {
      this.setState({
        billsList: response.data["data"],
      });
    });
  }

  render() {
    return (
      <div>
        {this.state.billsList.map((bill) => {
          return (
            <Card
              billId={bill.bill_id}
              billJIR={bill.jir}
              billPDV={bill.pdv}
              billPrice={bill.price}
              timeDate={bill.time_date}
              billZKI={bill.zki}
            />
          );
        })}
      </div>
    );
  }
}

export default BillsListAdmin;
