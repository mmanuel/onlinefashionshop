import React from 'react';
import {Accordion, Card} from "react-bootstrap";
import './css/Align.css'


function BillAdmin(props){
    return(
        <div className="izmedu">
        <Card style={{ width: "13rem" }} key={props.billId}>
            Bill ID {props.billId}
        <Accordion defaultActiveKey="0">
            <Card>
                <Accordion.Toggle as={Card.Header} eventKey="1">
                    JIR
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="1">
                    <Card.Body>{props.billJIR}</Card.Body>
                </Accordion.Collapse>
            </Card>
            <Card>
                <Accordion.Toggle as={Card.Header} eventKey="2">
                    PDV
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="2">
                    <Card.Body>{props.billPDV}</Card.Body>
                </Accordion.Collapse>
            </Card>
            <Card>
                <Accordion.Toggle as={Card.Header} eventKey="3">
                    Price
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="3">
                    <Card.Body>{props.billPrice}</Card.Body>
                </Accordion.Collapse>
            </Card>
            <Card>
                <Accordion.Toggle as={Card.Header} eventKey="4">
                    Time Date
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="4">
                    <Card.Body>{props.timeDate}</Card.Body>
                </Accordion.Collapse>
            </Card>
            <Card>
                <Accordion.Toggle as={Card.Header} eventKey="5">
                    ZKI
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="5">
                    <Card.Body>{props.billZKI}</Card.Body>
                </Accordion.Collapse>
            </Card>
        </Accordion>
        </Card>
        </div>
    )
}

export default BillAdmin;