import React, { Component } from "react";
import Card from "./ProductsAdmin";
import axios from "axios";

class ProductListAdmin extends Component {
  state = {
    productsList: [],
  };

  componentDidMount() {
    axios.get(window.backend + "/products").then((response) => {
      this.setState({
        productsList: response.data["data"],
      });
    });
  }

  render() {
    return (
      <div>
        {this.state.productsList.map((product) => {
          return (
            <Card
              productId={product.id}
              productName={product.product_name}
              productPrice={product.product_price}
              imgURL={product.img_url}
              description={product.desc}
              categoryName={product.category.category_name}
              categoryId={product.category.category_id}
              categoryIsActive={product.category.is_active}
              amount_S={product.amount.S}
              amount_M={product.amount.M}
              amount_L={product.amount.L}
              key={product.id}
            />
          );
        })}
      </div>
    );
  }
}

export default ProductListAdmin;
