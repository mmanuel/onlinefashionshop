import React, { Component } from "react";
import Info from "./Info";
import "./css/Align.css";

class InfoListAdmin extends Component {
  state = {
    id: "",
    firstName: "",
    lastName: "",
    email: "",
    role: "",
  };

  render() {
    return (
      <div>
        <Info
          id={localStorage.getItem("user_id")}
          firstName={localStorage.getItem("first_name")}
          lastName={localStorage.getItem("last_name")}
          email={localStorage.getItem("email")}
          role={localStorage.getItem("role")}
        />
      </div>
    );
  }
}

export default InfoListAdmin;
