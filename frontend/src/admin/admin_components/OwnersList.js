import React, { Component } from "react";
import Owner from "./Owner.js";
import axios from "axios";

class OwnersList extends Component {
  state = {
    ownerList: [],
  };
  componentDidMount() {
    const token = localStorage.getItem("token");
    axios
      .get(window.backend + "/users", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        this.setState({
          ownerList: response.data["data"],
        });
      });
  }

  render() {
    return (
      <div>
        {this.state.ownerList.map((owner) => {
          return (
            <Owner
              ownerId={owner.user_id}
              firstName={owner.first_name}
              lastName={owner.last_name}
              role={owner.role_name}
              key={owner.user_id}
            />
          );
        })}
      </div>
    );
  }
}

export default OwnersList;
