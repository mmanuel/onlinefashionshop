import React, { Component } from "react";
import {
  Alert,
  Button,
  Col,
  Container,
  Modal,
  Row,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";
import OwnersList from "./admin_components/OwnersList";
import Bar from "./Bar";
import Fab from "@material-ui/core/Fab";
import Tooltip from "@material-ui/core/Tooltip";
import Axios from "axios";
import axios from "axios";

class Owners extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAdd: false,
      usersList: [],
      user: "",
      id: "Select the person's first and last name",
      count: 0,
    };
  }
  handleSelectUser = (e) => {
    this.setState({ id: e });
    console.log(e);
    console.log(this.state.id);
  };
  handleSubmit = (event) => {
    event.preventDefault();
    const token = localStorage.getItem("token");

    Axios.put(
      window.backend + "/users",
      {
        user_id: this.state.id,
      },
      { headers: { Authorization: `Bearer ${token}` } }
    )
      .then((res) => {
        console.log(res);
        this.setState({ showDelete: false });
        window.location.reload(false);
      })
      .catch((err) => console.log(err));
  };
  componentDidMount() {
    const token = localStorage.getItem("token");
    axios
      .get(window.backend + "/users", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        this.setState({
          usersList: response.data["data"],
        });
        // eslint-disable-next-line
        this.state.usersList.map((user) => {
          if (user.role_name === "BasicUser") {
            this.setState({ count: this.state.count + 1 });
          }
        });
      });
  }

  render() {
    if (
      localStorage.getItem("token") != null &&
      localStorage.getItem("role") === "SystemAdmin"
    ) {
      const handleCloseAdd = () => this.setState({ showAdd: false });
      const handleShowAdd = () => this.setState({ showAdd: true });

      return (
        <div
          style={{
            paddingTop: "3rem",
            marginTop: "1rem",
            backgroundColor: "#fafcfc",
          }}
        >
          <Container fluid>
            <Row>
              <Bar />
              <Col>
                <Tooltip
                  onClick={handleShowAdd}
                  title="Add new owner!"
                  aria-label="add"
                  style={{ backgroundColor: "#88b8e7", marginTop: "1rem" }}
                >
                  <Fab color={"primary"} className="class">
                    <i className="fa fa-plus" aria-hidden="true">
                      {" "}
                    </i>
                  </Fab>
                </Tooltip>
                <OwnersList />
              </Col>
            </Row>
          </Container>
          <Modal
            show={this.state.showAdd}
            onHide={handleCloseAdd}
            backdrop="static"
            keyboard={false}
            centered
          >
            <Modal.Header closeButton onClick={handleCloseAdd}>
              <Modal.Title>Add Owner</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              {this.state.count > 0 && (
                <DropdownButton
                  class="dropdown"
                  title={this.state.id}
                  onSelect={this.handleSelectUser}
                  variant="outline-dark"
                  /*alignRight
                                title={this.state.size}
                                id="dropdown-menu-align-right"
                                onSelect={this.handleSelectSize}*/
                >
                  {this.state.usersList
                    .filter((role) => role.role_name === "BasicUser")
                    .map((user) => {
                      return (
                        <Dropdown.Item eventKey={user.user_id}>
                          {user.first_name} {user.last_name}
                        </Dropdown.Item>
                      );
                    })}
                </DropdownButton>
              )}
              {this.state.count === 0 && (
                <Alert variant="danger">No users to add.</Alert>
              )}
            </Modal.Body>
            <Modal.Footer>
              <Button
                onClick={handleCloseAdd}
                style={{ backgroundColor: "#e06666", borderColor: "#e06666" }}
              >
                Cancel
              </Button>
              <Button
                onClick={this.handleSubmit}
                style={{ backgroundColor: "#88b8e7", borderColor: "#88b8e7" }}
              >
                Add
              </Button>
            </Modal.Footer>
          </Modal>
        </div>
      );
    } else {
      return (
        <div
          style={{
            paddingTop: "3rem",
            marginTop: "1rem",
            backgroundColor: "#fafcfc",
          }}
        >
          <Container fluid>
            <Row>
              <Bar />
              <Col>
                <Alert variant="info">
                  {" "}
                  You don't have authority to manage shop owners
                </Alert>
                <OwnersList />
              </Col>
            </Row>
          </Container>
        </div>
      );
    }
  }
}

export default Owners;
