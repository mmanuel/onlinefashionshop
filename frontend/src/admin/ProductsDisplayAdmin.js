import React, { Component } from "react";
import {
  Button,
  Card,
  Col,
  Container,
  Dropdown,
  DropdownButton,
  Form,
  Modal,
  Row,
} from "react-bootstrap";
import ProductListAdmin from "./admin_components/ProductListAdmin";
import Bar from "./Bar";
import Fab from "@material-ui/core/Fab";
import Tooltip from "@material-ui/core/Tooltip";
import Axios from "axios";
import axios from "axios";
import RemoveIcon from "@material-ui/icons/Remove";
import AddIcon from "@material-ui/icons/Add";

class ProductsDisplayAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      description: "",
      price: "",
      size: "Select size",
      category: "Select category",
      img: "",
      showAdd: false,
      amountS: 0,
      amountM: 0,
      amountL: 0,
      categoriesList: [],
      selectedFile: null,
    };
  }

  handleChangeName = (event) => {
    this.setState({ name: event.target.value });
  };

  handleChangeDesc = (event) => {
    this.setState({ description: event.target.value });
  };

  handleChangePrice = (event) => {
    this.setState({ price: event.target.value });
  };

  handleChangeSize = (event) => {
    this.setState({ size: event });
    console.log(this.state.size);
  };
  handleChangeCategory = (event) => {
    this.setState({ category: event });
    console.log(this.state.category);
  };
  handleChangeImg = (event) => {
    this.setState({ img: event.target.value });
  };

  getFormData(object) {
    const formData = new FormData();
    Object.keys(object).forEach((key) => formData.append(key, object[key]));
    return formData;
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const token = localStorage.getItem("token");

    const data = this.getFormData({
      amount_L: this.state.amountL,
      amount_M: this.state.amountM,
      amount_S: this.state.amountS,
      product_name: this.state.name,
      product_price: this.state.price,
      desc: this.state.description,
      category_name: this.state.category,
      file: this.state.selectedFile == null ? null : this.state.selectedFile,
    });

    for (let pair of data.entries()) {
      console.log(pair[0] + ", " + pair[1]);
    }

    Axios.post(window.backend + "/products", data, {
      headers: {
        Authorization: `Bearer ${token}`,
        ContentType: "multipart/form-data",
      },
    })
      .then((res) => {
        console.log(res);
        this.setState({ showDelete: false });
        window.location.reload(false);
      })
      .catch((err) => console.log(err));
  };
  loadCategories() {
    axios.get(window.backend + "/categories").then((response) => {
      this.setState({
        categoriesList: response.data["data"],
      });
    });
  }

  render() {
    const handleCloseAdd = () => this.setState({ showAdd: false });
    const handleShowAdd = () => {
      this.setState({ showAdd: true });
      axios.get(window.backend + "/categories").then((response) => {
        this.setState({
          categoriesList: response.data["data"],
        });
      });
    };
    return (
      <div
        style={{
          paddingTop: "3rem",
          marginTop: "1rem",
          backgroundColor: "#fafcfc",
        }}
      >
        <Container fluid>
          <Row>
            <Bar />
            <Col>
              <Tooltip
                onClick={handleShowAdd}
                title="Add new product!"
                aria-label="add"
                style={{ backgroundColor: "#88b8e7", marginTop: "1rem" }}
              >
                <Fab color={"primary"} className="class">
                  <i className="fa fa-plus" aria-hidden="true">
                    {" "}
                  </i>
                </Fab>
              </Tooltip>
              <ProductListAdmin />
            </Col>
          </Row>
        </Container>

        <Modal
          show={this.state.showAdd}
          onHide={handleCloseAdd}
          backdrop="static"
          keyboard={false}
          centered
        >
          <Modal.Header closeButton onClick={handleCloseAdd}>
            <Modal.Title>Add Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group controlId="formBasicName">
                <Form.Label>Product name: </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter product name"
                  onChange={this.handleChangeName}
                />
              </Form.Group>
              <Form.Group controlId="formBasicDescription">
                <Form.Label>Description: </Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  placeholder="Enter description"
                  onChange={this.handleChangeDesc}
                />
              </Form.Group>
              <Form.Group controlId="formBasicPrice">
                <Form.Label>Price: </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter price"
                  onChange={this.handleChangePrice}
                />
              </Form.Group>
              <DropdownButton
                class="dropdown"
                title={this.state.category}
                onSelect={this.handleChangeCategory}
                variant="outline-dark"
              >
                {this.state.categoriesList.map((category) => {
                  return (
                    <Dropdown.Item eventKey={category.category_name}>
                      {category.category_name}
                    </Dropdown.Item>
                  );
                })}
              </DropdownButton>

              <Form.Group className="size_box">
                <Row>
                  <Card.Text style={{ marginLeft: "5%" }}>
                    Size S: {this.state.amountS}
                  </Card.Text>
                  <Col>
                    <Button
                      onClick={() => {
                        this.state.amountS > 0
                          ? this.setState({ amountS: this.state.amountS - 1 })
                          : this.setState({
                              amountS: this.state.amountS,
                            });
                      }}
                      variant="danger"
                      size="sm"
                      style={{
                        backgroundColor: "#e06666",
                        borderColor: "#e06666",
                        marginLeft: "20%",
                      }}
                    >
                      <RemoveIcon />
                    </Button>
                    <Button
                      onClick={() => {
                        this.setState({ amountS: this.state.amountS + 1 });
                      }}
                      variant="success"
                      size="sm"
                      style={{
                        backgroundColor: "#b0cebc",
                        borderColor: "#b0cebc",
                      }}
                    >
                      <AddIcon />
                    </Button>
                  </Col>
                </Row>
                <Row>
                  <Card.Text style={{ marginLeft: "5%" }}>
                    Size M: {this.state.amountM}
                  </Card.Text>
                  <Col>
                    <Button
                      onClick={() => {
                        this.state.amountM > 0
                          ? this.setState({ amountM: this.state.amountM - 1 })
                          : this.setState({
                              amountM: this.state.amountM,
                            });
                      }}
                      variant="danger"
                      size="sm"
                      style={{
                        backgroundColor: "#e06666",
                        borderColor: "#e06666",
                        marginLeft: "19%",
                      }}
                    >
                      <RemoveIcon />
                    </Button>
                    <Button
                      onClick={() => {
                        this.setState({ amountM: this.state.amountM + 1 });
                      }}
                      variant="success"
                      size="sm"
                      style={{
                        backgroundColor: "#b0cebc",
                        borderColor: "#b0cebc",
                      }}
                    >
                      <AddIcon />
                    </Button>
                  </Col>
                </Row>
                <Row>
                  <Card.Text style={{ marginLeft: "5%" }}>
                    Size L: {this.state.amountL}
                  </Card.Text>
                  <Col>
                    <Button
                      onClick={() => {
                        this.state.amountL > 0
                          ? this.setState({ amountL: this.state.amountL - 1 })
                          : this.setState({
                              amountL: this.state.amountL,
                            });
                      }}
                      variant="danger"
                      size="sm"
                      style={{
                        backgroundColor: "#e06666",
                        borderColor: "#e06666",
                        marginLeft: "20%",
                      }}
                    >
                      <RemoveIcon />
                    </Button>
                    <Button
                      onClick={() => {
                        this.setState({ amountL: this.state.amountL + 1 });
                      }}
                      variant="success"
                      size="sm"
                      style={{
                        backgroundColor: "#b0cebc",
                        borderColor: "#b0cebc",
                      }}
                    >
                      <AddIcon />
                    </Button>
                  </Col>
                </Row>
              </Form.Group>
              <div>
                <input
                  type="file"
                  onChange={(event) =>
                    this.setState({ selectedFile: event.target.files[0] })
                  }
                />
              </div>
              {this.state.selectedFile == null && (
                <Form.Text className="text-muted">
                  An image needs to be selected before submit.
                </Form.Text>
              )}
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button
              onClick={handleCloseAdd}
              style={{ backgroundColor: "#e06666", borderColor: "#e06666" }}
            >
              Cancel
            </Button>
            {this.state.selectedFile != null && (
              <Button
                onClick={this.handleSubmit}
                style={{ backgroundColor: "#88b8e7", borderColor: "#88b8e7" }}
              >
                Add
              </Button>
            )}
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default ProductsDisplayAdmin;
