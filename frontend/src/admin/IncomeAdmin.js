import React, { Component } from "react";
import axios from "axios";
import { Alert, Col, Container, Row } from "react-bootstrap";
import Bar from "./Bar";
import "./admin_components/css/Align.css";

class IncomeAdmin extends Component {
  state = {
    billsList: [],
  };

  componentDidMount() {
    axios.get(window.backend + "/bills").then((response) => {
      this.setState({
        billsList: response.data["data"],
      });
    });
  }
  incomeCalculate() {
    return this.state.billsList.reduce((a, v) => a + v.price, 0);
  }

  render() {
    const income = this.incomeCalculate();
    const nbOfBills = this.state.billsList.length;
    return (
      <div
        style={{
          paddingTop: "3rem",
          marginTop: "1rem",
          backgroundColor: "#fafcfc",
        }}
      >
        <Container fluid>
          <Row>
            <Bar />
            <div className="izmedu">
              <Col>
                <Alert variant="info"> This is income: {income}</Alert>
                <Alert variant="info"> Number of bills: {nbOfBills}</Alert>
              </Col>
            </div>
          </Row>
        </Container>
      </div>
    );
  }
}

export default IncomeAdmin;
