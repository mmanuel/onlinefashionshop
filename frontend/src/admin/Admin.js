
import React, {Component} from "react";
import Bar from "./Bar";
import {Row, Col, Image} from "react-bootstrap";
import "../components/css/HomePage.css";
import slika from "../images/admin.png";

class Admin extends Component {
    render() {
        return (
            <div style={{paddingTop:"3rem", marginTop: "1rem", backgroundColor:"#fafcfc"}}>
                <Row>
                    <Col xs ={4}><Bar/></Col>
                    <Col  style={{marginTop:"3rem"}}>
                        <Row ><Image src={slika} alt="service" height="600" width="600" style={{opacity:"0.9"}}/></Row>


                    </Col>
                </Row>
                </div>

        );
    }


}

export default Admin;
