import React, { Component } from "react";
import CouponsListAdmin from "./admin_components/CouponsListAdmin";
import { Button, Col, Container, Form, Modal, Row } from "react-bootstrap";
import Bar from "./Bar";
import Fab from "@material-ui/core/Fab";
import Tooltip from "@material-ui/core/Tooltip";
import Axios from "axios";

class CouponsAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAdd: false,
      amount: 0,
      code: "",
      expires: "",
      percetage: 0,
    };
  }
  handleChangeAmount = (event) => {
    this.setState({ amount: event.target.value });
  };
  handleChangeCode = (event) => {
    this.setState({ code: event.target.value });
  };
  handleChangeExpires = (event) => {
    this.setState({ expires: event.target.value });
  };
  handleChangePercentage = (event) => {
    this.setState({ percetage: event.target.value });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    const token = localStorage.getItem("token");

    Axios.post(
      window.backend + "/coupons",
      {
        amount: this.state.amount,
        code: this.state.code,
        expires: this.state.expires,
        percentage: this.state.percetage,
      },
      { headers: { Authorization: `Bearer ${token}` } }
    )
      .then((res) => {
        console.log(res);
        this.setState({ showDelete: false });
        window.location.reload(false);
      })
      .catch((err) => console.log(err));
  };
  render() {
    const handleCloseAdd = () => this.setState({ showAdd: false });
    const handleShowAdd = () => this.setState({ showAdd: true });
    return (
      <div
        style={{
          paddingTop: "3rem",
          marginTop: "1rem",
          backgroundColor: "#fafcfc",
        }}
      >
        <Container fluid>
          <Row>
            <Bar />
            <Col>
              <Tooltip
                onClick={handleShowAdd}
                title="Add new coupon!"
                aria-label="add"
                style={{ backgroundColor: "#88b8e7", marginTop: "1rem" }}
              >
                <Fab color={"primary"} className="class">
                  <i className="fa fa-plus" aria-hidden="true">
                    {" "}
                  </i>
                </Fab>
              </Tooltip>
              {/*<Button onClick={handleShowAdd}>Add new coupon!</Button>*/}
              <CouponsListAdmin />
            </Col>
          </Row>
        </Container>
        <Modal
          show={this.state.showAdd}
          onHide={handleCloseAdd}
          backdrop="static"
          keyboard={false}
          centered
        >
          <Modal.Header closeButton onClick={handleCloseAdd}>
            <Modal.Title>Add Coupon</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group controlId="formBasicAmount">
                <Form.Label>Amount: </Form.Label>
                <Form.Control
                  type="number"
                  placeholder="Enter amount"
                  onChange={this.handleChangeAmount}
                />
              </Form.Group>
              <Form.Group controlId="formBasicCode">
                <Form.Label>Code: </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter code"
                  onChange={this.handleChangeCode}
                />
              </Form.Group>
              <Form.Group controlId="formBasicDate">
                <Form.Label>Expires: </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter expiry date"
                  onChange={this.handleChangeExpires}
                />
              </Form.Group>
              <Form.Group controlId="formBasicPercentage">
                <Form.Label>Percentage: </Form.Label>
                <Form.Control
                  type="number"
                  placeholder="Enter percentage"
                  onChange={this.handleChangePercentage}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button
              onClick={handleCloseAdd}
              style={{ backgroundColor: "#e06666", borderColor: "#e06666" }}
            >
              Cancel
            </Button>
            <Button
              onClick={this.handleSubmit}
              style={{ backgroundColor: "#88b8e7", borderColor: "#88b8e7" }}
            >
              Add
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default CouponsAdmin;
