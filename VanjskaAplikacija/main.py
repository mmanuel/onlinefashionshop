from flask import Flask, jsonify, request
import uuid

app = Flask(__name__)


@app.route('/')
def home():
    return jsonify(welcome="hi")


@app.route('/jir')
def jir():
    jir = uuid.uuid4()
    return jsonify(jir=jir)


@app.route('/zki')
def zki():
    zki = uuid.uuid4().hex
    return jsonify(zki=zki)


@app.route('/getTime')
def getTime():
    time = request.args.get("time")
    return jsonify(time=time)


if __name__ == '__main__':
    app.run()
